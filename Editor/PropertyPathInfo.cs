﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Text;
using Object = UnityEngine.Object;

namespace PainfulSmile
{
    public sealed class PropertyPathInfo
    {
        private readonly int? Index;
        private readonly FieldInfo FieldInfo;
        private readonly PropertyPathInfo Next;

        internal PropertyPathInfo(FieldInfo fieldInfo, PropertyPathInfo next, int? index = null)
        {
            FieldInfo = fieldInfo;
            Next = next;
            Index = index;
        }

        public override string ToString()
        {
            var builder = new StringBuilder();
            PropertyPathInfo current = this;

            do
            {
                builder.Append($"{current.FieldInfo.Name}");

                if (current.Index.HasValue)
                {
                    builder.Append($"[{current.Index}]");
                }

                current = current.Next;

                if (current == null)
                {
                    break;
                }

                builder.Append(".");
            } while (true);

            return builder.ToString();
        }

        public object GetParentValue(Object target)
        {
            object value = target;

            for (PropertyPathInfo current = this; current.Next != null; current = current.Next)
            {
                if (current.Index.HasValue == false)
                {
                    value = current.FieldInfo.GetValue(value);
                }
                else
                {
                    IEnumerator enumerator = ((IEnumerable)current.FieldInfo.GetValue(value)).GetEnumerator();

                    for (int i = 0; enumerator.MoveNext(); i++)
                    {
                        if (current.Index == i)
                        {
                            value = enumerator.Current;

                            break;
                        }
                    }
                }
            }

            return value;
        }

        public T GetParentValue<T>(Object target)
        {
            object value = GetParentValue(target);

            return value != null ? (T)value : default;
        }

        public void GetParentValues(Object[] targets, List<object> append)
        {
            int capacity = append.Count + targets.Length;

            if (append.Capacity < capacity)
            {
                append.Capacity = capacity;
            }

            for (int i = 0; i < targets.Length; i++)
            {
                append.Add(GetParentValue(targets[i]));
            }
        }

        public object[] GetParentValues(Object[] targets)
        {
            var values = new object[targets.Length];

            for (int i = 0; i < targets.Length; i++)
            {
                values[i] = GetParentValue(targets[i]);
            }

            return values;
        }

        public void GetParentValues<T>(Object[] targets, List<T> append)
        {
            int capacity = append.Count + targets.Length;

            if (append.Capacity < capacity)
            {
                append.Capacity = capacity;
            }

            for (int i = 0; i < targets.Length; i++)
            {
                append.Add(GetParentValue<T>(targets[i]));
            }
        }

        public T[] GetParentValues<T>(Object[] targets)
        {
            var values = new T[targets.Length];

            for (int i = 0; i < targets.Length; i++)
            {
                values[i] = GetParentValue<T>(targets[i]);
            }

            return values;
        }

        public object GetValue(Object target)
        {
            return GetValueInternal(target);
        }

        public T GetValue<T>(Object target)
        {
            object value = GetValue(target);

            return value != null ? (T)value : default;
        }

        public void GetValues<T>(Object[] targets, List<T> append)
        {
            int capacity = append.Count + targets.Length;

            if (append.Capacity < capacity)
            {
                append.Capacity = capacity;
            }

            for (int i = 0; i < targets.Length; i++)
            {
                append.Add(GetValue<T>(targets[i]));
            }
        }

        public T[] GetValues<T>(Object[] targets)
        {
            var values = new T[targets.Length];

            for (int i = 0; i < targets.Length; i++)
            {
                values[i] = GetValue<T>(targets[i]);
            }

            return values;
        }

        public void SetValue<T>(Object target, T value)
        {
            SetValueInternal(target, value);
        }

        public void SetValue<T>(Object target, Func<T, T> onSetValue)
        {
            var oldValue = GetValue<T>(target);
            T newValue = onSetValue.Invoke(oldValue);
            SetValue(target, newValue);
        }

        public void SetValues<T>(Object[] targets, T value)
        {
            foreach (Object target in targets)
            {
                SetValue(target, value);
            }
        }

        public void SetValues<T>(Object[] targets, Func<T, T> onSetValue)
        {
            foreach (Object target in targets)
            {
                SetValue(target, onSetValue);
            }
        }

        private object GetValueInternal(object target)
        {
            object value = null;

            if (Index.HasValue == false)
            {
                value = FieldInfo.GetValue(target);
            }
            else
            {
                IEnumerator enumerable = ((IEnumerable)FieldInfo.GetValue(target)).GetEnumerator();

                for (int i = 0; enumerable.MoveNext(); i++)
                {
                    if (Index == i)
                    {
                        value = enumerable.Current;

                        break;
                    }
                }
            }

            return Next != null ? Next.GetValueInternal(value) : value;
        }

        private void SetValueInternal<T>(object obj, T value)
        {
            object target = obj;
            PropertyPathInfo current = this;

            while (current.Next != null)
            {
                if (current.Index.HasValue == false)
                {
                    target = current.FieldInfo.GetValue(target);
                }
                else
                {
                    IEnumerator enumerator = ((IEnumerable)current.FieldInfo.GetValue(target)).GetEnumerator();

                    for (int i = 0; enumerator.MoveNext(); i++)
                    {
                        if (current.Index == i)
                        {
                            target = enumerator.Current;

                            break;
                        }
                    }
                }

                current = current.Next;
            }

            if (current.Index.HasValue == false)
            {
                current.FieldInfo.SetValue(target, value);
            }
            else
            {
                var array = current.FieldInfo.GetValue(target) as T[];

                if (array == null)
                {
                    return;
                }

                if (array.Length > current.Index)
                {
                    array[current.Index.Value] = value;
                }
                else
                {
                    var temp = new List<T>(array);
                    temp.Add(value);
                    array = temp.ToArray();
                }

                current.FieldInfo.SetValue(target, array);
            }
        }
    }
}
