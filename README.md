# SerializedPropertyFieldReference

Utility to make easier to create a `PropertyDrawer` with multi-edit support.

```c#
public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
{
    // Defaults a string field to "<none>"
    string ValidateEmptyFields(string currentValue)
    {
        string newValue = currentValue;

        if (string.IsNullOrEmpty(newValue))
        {
            newValue = "<none>";
        }

        return newValue;
    }

    property.SetValues(ValidateEmptyFields);
}
```

## See also

- [Changelog](CHANGELOG.md)
- [License](LICENSE.md)
- [Author](https://gitlab.com/brunocoimbra)
